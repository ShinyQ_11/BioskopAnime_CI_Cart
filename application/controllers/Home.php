<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller{


  function index()
  {
      $this->load->model('m_film');
      $data['tampil_film']=$this->m_film->tampil_film();
      $data['konten']='index';
      if ($this->session->userdata('login')==TRUE) {
        $data['loginmuncul']="login-mucul";
        $data['butLogout']=" Log out";
        $data['cartmuncul']="cartmuncul";
        $data['logoutLink']="/index.php/home/logout";
        $data['iconLogout']="glyphicon glyphicon-log-out";
      }else {
        $data['loginmuncul']="login-hide";
        $data['logoutLink']="/index.php/home/register";
        $data['butLogout']=" Daftar";
        $data['cartmuncul']="carthilang";
        $data['iconLogout']="glyphicon glyphicon-edit";
      }
      $this->load->view('template',$data);

  }

  public function template()
  {
    $data['konten']="template";
    $this->load->view('template', $data);
  }
  function pesan_tiket($id_film=''){
    $this->load->model('m_film','video');
    $data['konten']="pesan_tiket";
		$data['judul']="Pesan Tiket";

    if ($this->session->userdata('login')==TRUE) {
      $data['konten']='pesan_tiket';
      $data['loginmuncul']="login-mucul";
      $data['cartmuncul']="cartmuncul";
      $data['butLogout']=" Log out";
      $data['logoutLink']="/index.php/home/logout";
      $data['iconLogout']="glyphicon glyphicon-log-out";
      $data['detail']=$this->video->detail($id_film);
      $this->load->view('template', $data);
    }else{
      redirect('home/login','refresh');
    }
  }

  public function detail($id_film='')
	{
		$this->load->model('m_film','video');

		$data['konten']="detail";
		$data['judul']="Detail Film";
    if ($this->session->userdata('login')==TRUE) {
      $data['loginmuncul']="login-mucul";
      $data['butLogout']=" Log out";
      $data['cartmuncul']="cartmuncul";
      $data['logoutLink']="/index.php/home/logout";
      $data['iconLogout']="glyphicon glyphicon-log-out";
    } else {
      $data['loginmuncul']="login-hide";
      $data['logoutLink']="/index.php/home/register";
      $data['butLogout']=" Daftar";
      $data['iconLogout']="glyphicon glyphicon-log-in";
    }

		$data['detail']=$this->video->detail($id_film);
		$this->load->view('template', $data);
	}


  public function login()
	{
    $data['konten']="login";
    $data['judul']="Login";
		$this->load->view('login',$data);
	}

	public function register()
	{
		$this->load->view('register');
	}

	public function simpan()
	{
		if ($this->input->post('register')) {
			$this->form_validation->set_rules('nama_user', 'Nama Lengkap', 'trim|required');
			$this->form_validation->set_rules('nomor_user', 'Nomor', 'trim|required');
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
				if ($this->form_validation->run()==TRUE) {
					$this->load->model(array('m_user'));
					if ($this->m_user->masuk() == true) {
							$this->session->set_flashdata('pesan',validation_errors());
							redirect('Home/login','refresh');
					}
				}else {
					$this->session->set_flashdata('pesan',validation_errors());
					redirect('Home/register');
				}
			}
  }

	public function proses_login()
	{
		if ($this->input->post('login')) {
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$this->load->model(array('m_user'));
				if ($this->m_user->get_login()->num_rows()>0) {
					$data=$this->m_user->get_login()->row();
					$array = array(
						'login' => TRUE,
						'username'=> $data->username,
						'password'=> $data->password,
            'nama_user'=>$data->nama_user,
            'id_user'=>$data->id_user
					);
					$this->session->set_userdata($array);
					redirect('home','refresh');
				}else {
					$this->session->set_flashdata('pesan','Salah Username dan Password');
					redirect('Home/login','refresh');
				}
			}else {
				$this->session->set_flashdata('pesan',validation_errors());
				redirect('home/login');
			}
		}

	}
  public function logout()
  {
    $this->session->sess_destroy();
    redirect('home/login','refresh');
  }

}
