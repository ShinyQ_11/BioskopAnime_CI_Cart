<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('m_cart','crt');
  }

  function index()
  {
    if ($this->session->userdata('login')==TRUE) {
      $data['loginmuncul']="login-mucul";
      $data['butLogout']=" Log out";
      $data['cartmuncul']="cartmuncul";
      $data['logoutLink']="/index.php/home/logout";
      $data['iconLogout']="glyphicon glyphicon-log-out";
    } else {
      $data['loginmuncul']="login-hide";
      $data['logoutLink']="/index.php/home/register";
      $data['butLogout']=" Daftar";
      $data['iconLogout']="glyphicon glyphicon-log-in";
    }
    $data['konten']="showcart";
    $data['judul']="Cart";
    $this->load->view('template', $data);
  }

  public function add_cart($id_film)
  {
    $nama=$this->input->post('nama_user');
    $email=$this->input->post('email');
    $jumlah=$this->input->post('jumlah');

    $this->load->model('m_film');
    $detail=$this->m_film->detail($id_film);

    $data = array(
        'id'      => $detail->id_film,
        'qty'     => $jumlah,
        'price'   => $detail->harga,
        'name'    => $detail->nama_film,
        'options' => array('nama_user'=>$nama,'email'=>$email)
      );

      $this->cart->insert($data);
      redirect('home/pesan_tiket/'.$id_film);
  }
  public function simpan()
  {
    if ($this->input->post('simpan')) {
      $this->load->model('m_cart');
      $id_nota=$this->m_cart->simpan_cart();
      if ($id_nota>0) {
        $this->cart->destroy();
        redirect('cart/pembayaran/'.$id_nota,'refresh');
      }else{
        redirect('cart');
      }
    }
  }
  public function pembayaran($id_film)
  {
    $this->load->model('m_cart');
    $nota = $this->m_cart->get_total($id_film);
    if ($this->session->userdata('login')==TRUE) {
      $data['loginmuncul']="login-mucul";
      $data['butLogout']=" Log out";
      $data['cartmuncul']="cartmuncul";
      $data['logoutLink']="/index.php/home/logout";
      $data['iconLogout']="glyphicon glyphicon-log-out";
    } else {
      $data['loginmuncul']="login-hide";
      $data['logoutLink']="/index.php/home/register";
      $data['butLogout']=" Daftar";
      $data['iconLogout']="glyphicon glyphicon-log-in";
    }
    $data['total']=$nota->grandtotal+$id_film;
    $data['konten']="v_pembayaran";
    $this->load->view('template', $data, FALSE);
  }
  public function konfirm($id_nota)
  {
    $data['id_nota']=$id_nota;
    if ($this->session->userdata('login')==TRUE) {
      $data['loginmuncul']="login-mucul";
      $data['butLogout']=" Log out";
      $data['cartmuncul']="cartmuncul";
      $data['logoutLink']="/index.php/home/logout";
      $data['iconLogout']="glyphicon glyphicon-log-out";
    } else {
      $data['loginmuncul']="login-hide";
      $data['logoutLink']="/index.php/home/register";
      $data['butLogout']=" Daftar";
      $data['iconLogout']="glyphicon glyphicon-log-in";
    }
    $data['konten']="v_konfirm";
    $this->load->view('template', $data, FALSE);

  }
  public function proses_upload()
  {

    $config['upload_path'] = './assets/bukti/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size']  = '10000';

    $this->load->library('upload', $config);

    if ( ! $this->upload->do_upload('bukti')){
      $error = array('error' => $this->upload->display_errors());

      $this->session->flashdata('pesan', $this->upload->display_errors());

      redirect('cart/konfirm/'.$this->input->post('id_nota'),'refresh');


    }
    else{
      if($this->crt->update_bukti($this->upload->data('file_name'))){
        $this->session->set_flashdata('pesan', 'sukses upload bukti pembayaran');
        redirect('cart/konfirm/'.$this->input->post('id_nota'),'refresh');
      }
    }

  }
  public function hapus_item($id)
  {
    $data = array(
      'rowid'   => $id,
      'qty'     => 0,
    );

    $this->cart->update($data);
    redirect('cart','refresh');
  }
}


 ?>
