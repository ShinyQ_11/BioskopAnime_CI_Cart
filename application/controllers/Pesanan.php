
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pesanan extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_pesan','psn');
  }

  function index()
  {
    $data['pesanan']=$this->psn->tm_pesan();

    $data['konten']="v_pesanan";
    if ($this->session->userdata('login')==TRUE) {
      $data['loginmuncul']="login-mucul";
      $data['butLogout']=" Log out";
      $data['cartmuncul']="cartmuncul";
      $data['logoutLink']="/index.php/home/logout";
      $data['iconLogout']="glyphicon glyphicon-log-out";
    } else {
      $data['loginmuncul']="login-hide";
      $data['logoutLink']="/index.php/home/register";
      $data['butLogout']=" Daftar";
      $data['iconLogout']="glyphicon glyphicon-log-in";
    }
    $this->load->view('template', $data, FALSE);
  }
  public function hapus($id)
  {
    $this->load->model('m_pesan');
    $nota = $this->m_pesan->hapus($id);
    redirect('pesanan','refresh');
  }

} ?>
