<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/css/animate.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/style.css">
    <script src="<?=base_url()?>/assets/js/jquery.js"></script>
    <script src="<?=base_url()?>/assets/js/animasi.js"></script>
    <script src="<?=base_url()?>/assets/js/bootstrap.js"></script>
    <title>AANIME | Official Website</title>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
                <a class="navbar-brand" href="<?=base_url()?>index.php/home/index">
                  AANIME
                </a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?=base_url()?>index.php/home/index"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                    <li>
                        <a href="<?=base_url()?>index.php/pesanan"><span class="glyphicon glyphicon-calendar"></span> Pesanan</span></a>
                      </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li class="<?=$cartmuncul?>">
                    <a href="<?=base_url()?>/index.php/cart/" >
                      <i class="glyphicon glyphicon-shopping-cart" ></i>
                        Cart
                      <span class="label label-success" style="margin-left:6px;">
                        <?=$this->cart->total_items();?>
                      </span>
                    </a>
                  </li>
                    <li class="<?=$loginmuncul?>">
                        <a href="<?=base_url()?>/index.php/home/login"> <span class="glyphicon glyphicon-log-in"></span> Login</a>
                    </li>
                    <li><a href="<?=base_url()?><?=$logoutLink?>"><span class="<?=$iconLogout?>"></span><?=$butLogout?></a></li>
                </ul>
            </div>
        </div>
    </nav>
<?php
  $this->load->view($konten);
 ?>
