<div class="container-fluid" style="margin-top:70px;padding:20px; background:#eee;">
<center>
    <h2>Upload data Bukti Pembayaran</h2>
    <?php if($this->session->flashdata('pesan')):?>
        <div class="alert alert-warning">
            <?=$this->session->flashdata('pesan');?>
        </div>
    <?php endif?>

    <form action="<?=base_url('index.php/cart/proses_upload')?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="id_nota" value="<?=$id_nota?>">

        <input type="file" name="bukti" class="form_control">

        <input type="submit" name="upload" value="UPLOAD" class="btn btn-success" style="margin-top:20px;">
    </form>
    </center>
</div>
