<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=base_url()?>/assets/css/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url()?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>/assets/css/animate.css">
    <link rel="stylesheet" href="<?=base_url()?>/assets/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/animasi.js"></script>
    <script src="js/bootstrap.js"></script>
    <meta charset="utf-8">
    <title><?=$judul?></title>
  </head>
  <body style="background-image:url('<?=base_url()?>/assets/images/bg-login.jpg');">
    <div class="container-fluid loginanu">
      <center>

      <section id="login">
      <div class="container">
      	<div class="row loginbg">
      	    <div class="col-md-12 siplogin">
                  <h1>Login Dulu Gan</h1>
                    <div class="col-md-12">
                      <?php
                        if ($this->session->flashdata('pesan')!=NULL) {
                      ?>
                      <div class="alert alert-danger">
                        <?=$this->session->flashdata('pesan');?>
                      </div>
                      <?php
                        }
                       ?>

                    </div>
                      <form role="form" action="<?=base_url('index.php/home/proses_login')?>" method="post" id="login-form" autocomplete="off">
                          <div class="form-group">
                              <label for="email" class="sr-only">Username</label>
                              <input type="text" name="username" class="form-control" placeholder="Username">
                          </div>
                          <div class="form-group">
                              <label for="key" class="sr-only">Password</label>
                              <input type="password" name="password" class="form-control" placeholder="Password">
                          </div>
                          <input type="submit" name="login" class="btn btn-custom btn-lg btn-block" value="Log in" style="textWeight:bolder;"></input>
                      </form>
                      <div class="col-lg-12" style="margin-bottom:20px;">
                        <a href="<?=base_url('index.php/home/register')?>" class="text-center" style="color:white;">Register a new membership</a>
                      </div>
      		</div>
      	</div>
      </div>
  </section>
  </center>
    </div>
  </body>
</html>
