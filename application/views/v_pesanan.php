<div class="container-fluid" style="margin:60px; padding:40px; background:#eee; border-radius:20px;">
  <h2 style="font-weight:bold; margin-botton:20px;">Daftar Pesanan Saya</h2>

  <table class="table table-hober table-striped">
    <tr>
      <td>No</td>
      <td>No Nota</td>
      <td>Grand Total</td>
      <td>Status</td>
      <td>Konfirm</td>
      <td>Detail</td>
    </tr>
    <?php
      $no = 0;
      foreach ($pesanan as $psn):$no++
    ?>
    <tr>
      <td><?=$no?></td>
      <td><?=$psn->id_nota?></td>
      <td><?=$psn->grandtotal?></td>
      <td><?=$psn->status?></td>
      <td><?php
        if ($psn->status==""):?>
        <a href="<?=base_url('index.php/cart/konfirm/'.$psn->id_nota)?>">Konfirmasi</a> |
        <a href="<?=base_url('index.php/pesanan/hapus/'.$psn->id_nota)?>">Cancel</a>
      <?php else: ?>
        LUNAS
      <?php endif ?>
      </td>
      <td>Lihat Barang</td>
    </tr>
  <?php endforeach ?>
  </table>
</div>
