    <div id="mydiv" class="hero tes">
        <div class="nerawang" style="height:100vh" id="loss">
            <div id="rumah" class="anu">
                <div class="hero-caption">
                    <div class="bounceInDown">
                        <h1>The Place Where Anime is Micin</h1>
                    </div>
                    <div class="flipInY">
                        <p>
                          Welcome  <?php echo $this->session->userdata('nama_user');?>
                        </p>

                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="margin-top:100px">
                <a href="<?=base_url()?>/assets/#pengertian" style="color:white;"><i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid k1-1">
      <h1>1</h1>
    </div>
    <div class="container-fluid k1">
        <div class="quotes">
            <h1>Official Website To Watch Anime Movie</h1>
            <hr>
        </div>
    </div>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <img src="<?=base_url()?>/assets/images/1-1.png" alt="Los Angeles">
            </div>

            <div class="item">
                <img src="<?=base_url()?>/assets/images/2.jpg" alt="Chicago">
            </div>

            <div class="item">
                <img src="<?=base_url()?>/assets/images/3.jpg" alt="New york">
            </div>
        </div>
        <a class="left carousel-control" href="<?=base_url()?>/assets/#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="<?=base_url()?>/assets/#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="container-fluid konten" id="todayfilm">
        <div class="row">
            <div class="panel panel-default">
              <div class="container-fluid k2">
                  <h1>Today Movies</h1>
              </div>
                <div class="panel-body filmtop">
                    <div class="row">
                      <?php
                      foreach ($tampil_film as $film) {
                          ?>
                          <div class="col-md-3  konten1">
                              <div class="thumbnail">
                                  <div class="caption">
                                      <img src="<?=base_url()?>/assets/images/fil/<?=$film->gambar_film?>" alt="" class="img-rounded">
                                      <a href="<?=base_url()?>index.php/home/detail/<?=$film->id_film?>" role="button"><h4><?=$film->nama_film?></h4></a>
                                      <center><h5><?=$film->jam?></h5>
                                        <div class="btn-group">
                                        <a href="<?=base_url()?>index.php/home/pesan_tiket/<?=$film->id_film?>"><button type="button" class="btn btn-default">Pesan Tiket</button></a>
                                        </div>
                                      </center>
                                  </div>
                              </div>
                          </div>
                        <?php
                      }
                       ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php
    foreach ($tampil_film as $film) {
      ?>
      <div class="modal fade" id="modalFilm<?=$film->id_film?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                      <h4 class="modal-title" id="myModalLabel"><?=$film->nama_film?></h4>
                  </div>
                  <div class="modal-body">
                      <p><?=$film->sinopsis_film?></p>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </div>
      <?php
    }
     ?>
    <script type="text/javascript">
    </script>
</body>
</html>
