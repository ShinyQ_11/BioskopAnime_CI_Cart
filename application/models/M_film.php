<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_film extends CI_Model{

  public function tampil_film()
  {
    return $this->db->get('film')->result();
  }

  public function detail($id_film)
  {
    return $this->db
    ->where('id_film',$id_film)
    ->get('film')
    ->row();
  }
}
