<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pesan extends CI_Model{

  public function __construct()
  {
    parent::__construct();

  }
  public function tm_pesan()
  {
    return $this->db->where('id_user', $this->session->userdata('id_user'))->get('nota')->result();
  }

  public function hapus($id_nota)
  {
    $this->db->delete('pembelian',array('id_nota' => $id_nota ));
    $this->db->delete('nota',array('id_nota' => $id_nota ));
  }

}
 ?>
